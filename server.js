const $config = require('config');
const $express = require('express');
const $Templates = require('./src/modules/templates');

const SERVER_PORT = process.env.PORT || $config.get('server.port');

const middlewares = $config.get('server.middlewares');
const handlers = $config.get('server.handlers');

const app = $express();

middlewares.forEach((name) => {
  require('./src/middlewares/' + name)(app);
});

handlers.forEach((name) => {
  require('./src/handlers/' + name)(app);
});

$Templates.setCache();

app.listen(SERVER_PORT, () => {
  console.log('DTP Frontend listing :%s port', SERVER_PORT);
});