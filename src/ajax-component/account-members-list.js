const $API = require('../modules/api');
const $Templates = require('../modules/templates');
const $config = require('config');

module.exports = async function (req) {
  const token = req.cookies.dtptoken;

  if (!token) {
    return 'invalid token';
  }

  const accountId = req.body.account_id;
  const userId = req.session.user.id;

  if (!accountId) {
    return 'invalid acconut_id';
  }
  
  const list = await $API.query('getAccountMembers/' + accountId, {
    token
  });

  if (!list.ok) {
    throw list;
  }

  let html;
  const userMember = list.result.find((member) => member.id === userId);
  const isAdmin = userMember.access === 2;

  if (list.result.length !== 0) {
    html = $Templates.render('component-account-members', {
      list: list.result.map((member) => {
        member.me = (member.id === userId) && !isAdmin;
        member.isAdmin = isAdmin && (member.id !== userId);

        if (!member.photo) {
          member.textPhoto = member.first_name[0];
        } else {
          const photo =  member.photo[0].file_id;
          member.uPhoto = $config.get('services.files.url') + '/photo/' + photo;
        }
  
        return member;
      })
    });
  } else {
    html = '';
  }

  return {
    ok: true,
    html
  };
};