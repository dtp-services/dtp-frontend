const $API = require('../modules/api');
const $Templates = require('../modules/templates');

module.exports = async function (req) {
  const token = req.cookies.dtptoken;

  if (!token) {
    return 'invalid token';
  }
  
  const accounts = await $API.query('getAccounts', {
    token
  });

  if (!accounts.ok) {
    throw accounts;
  }

  let html;

  if (accounts.result.length !== 0) {
    html = $Templates.render('component-manage-account-list', {
      list: accounts.result.map((item) => {
        item.textPhoto = item.short_name[0];
        item.isEditor = item.access === 1;
        item.isOwner = item.access === 2;

        return item;
      })
    });
  } else {
    html = $Templates.render('component-empty-accounts');
  }

  return {
    ok: true,
    html,
    json: accounts.result
  };
};