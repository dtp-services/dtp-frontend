const _ = require('lodash');

const ADMINS = require('../../config/admins');

module.exports = function (req, res, next) {
  if (!ADMINS[_.get(req, 'session.user.id', '-1')]) {
    return res.redirect('/');
  }

  _.set(req, 'session.admin', ADMINS[_.get(req, 'session.user.id', '-1')]);

  next();
};