const $Session = require('../modules/session');
const $log = require('../libs/log');

module.exports = function (app) {
  app.use(function (req, res, next) {
    if (!req.cookies || !req.cookies.dtptoken) {
      return next();
    }

    const sessionid = req.cookies.dtptoken;

    $Session.get(sessionid)
      .then((data) => {
        if (!data) {
          return;
        }

        req.session = data;
      })
      .then(() => {
        next();
      })
      .catch((err) => {
        $log.error(err);
        res.end('Server Error!');
      });
  });
};