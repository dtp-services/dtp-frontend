const $Pages = require('../modules/pages');

module.exports = function (app) {
  app.get('/', (req, res) => {
    $Pages.render('home', {
      query: req.query,
      body: req.body,
      cookies: req.cookies,
      session: req.session
    })
      .then((content) => {
        res.end(content);
      })
      .catch((err) => {
        console.log(err);

        if (err === 'INVALID_TOKEN') {
          res.clearCookie('dtptoken');
          res.redirect('/');

          return;
        }

        res.status(502).end('Unknown error...');
      });
  });
};