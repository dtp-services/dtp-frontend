const $Pages = require('../modules/pages');

module.exports = function (app) {
  app.get('/about', (req, res) => {
    $Pages.render('about')
      .then((data) => {
        res.end(data);
      })
      .catch((err) => {
        console.log(err);
        res.status(502).end('Unknown error...');
      })
  });
};