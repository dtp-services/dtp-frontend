const $Pages = require('../modules/pages');

module.exports = function (app) {
  app.get('/*', (req, res, next) => {
    const shortId = req.params[0];

    $Pages.render('post', {
      shortId,
      cookies: req.cookies
    })
      .then((data) => {
        res.end(data);
      })
      .catch((err) => {
        if (err === 'PAGE_NOT_FOUND') {
          return next();
        }

        console.log(err);
        res.status(502).end('Unknown error...');
      })
  });
};