const $Pages = require('../modules/pages');

module.exports = function (app) {
  app.get('/tune', (req, res) => {
    if (!req.session) {
      res.redirect('/');
    }

    $Pages.render('tune', {
      cookies: req.cookies,
      session: req.session
    })
      .then((data) => {
        res.end(data);
      })
      .catch((err) => {
        console.log(err);
        res.status(502).end('Unknown error...');
      })
  });
};