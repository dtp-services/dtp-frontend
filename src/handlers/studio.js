const $Pages = require('../modules/pages');

module.exports = function (app) {
  app.get('/cs', (req, res) => {
    if (!req.session) {
      return res.redirect('/');
    }

    $Pages.render('studio', {
      cookies: req.cookies,
      session: req.session
    })
      .then((data) => {
        res.end(data);
      })
      .catch((err) => {
        console.log(err);
        res.status(502).end('Unknown error...');
      });
  });
};