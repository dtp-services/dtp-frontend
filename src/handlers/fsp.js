const $request = require('request');
const $qs = require('qs');

module.exports = function (app) {
  app.get('/fsp/*', (req, res) => {
    const path = req.params[0];
    const source = `http://telegra.ph/${path}?${$qs.stringify(req.query)}`;

    res.writeHead(200, {
      'Cache-Control': 'max-age=2678400000'
    });

    $request
      .get(source)
      .pipe(res);
  });
};