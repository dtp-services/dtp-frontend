const $API = require('../modules/api');
const $log = require('../libs/log');

module.exports = function (app) {
  app.get('/telegraph/*', (req, res) => {
    const token = req.cookies.dtptoken;
    const accountId = req.params[0];

    $API.query('getAccount/' + accountId, {
      token
    })
      .then(async (data) => {
        if (!data.ok) {
          throw data;
        }

        res.redirect(data.result.auth_url);
      })
      .catch((err) => {
        $log.error(err);
        res.end('¯\_(ツ)_/¯');
      });
  });
};