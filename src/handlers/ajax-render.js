const $log = require('../libs/log');
const _ = require('lodash');

module.exports = function (app) {
  app.post('/ajaxRender/*', (req, res) => {
    const componentName = req.params[0];
    let component;

    try {
      component = require('../ajax-component/' + componentName);
    } catch (err) {
      component = false;
    }

    if (!component) {
      return res.status(404).end('Component is not found');
    }

    component(req)
      .then((result) => {
        if (typeof result === 'object') {
          res.json(result);
        } else {
          res.end(result);
        }
      })
      .catch((err) => {
        if (err === 'INVALID_TOKEN') {
          res.clearCookie('dtptoken');
          res.redirect('/');

          return;
        } else if (_.get(err, 'error.code')) {
          return res.json(err);
        }
                        
        $log.error(err);
        res.end('Server Error!');
      });
  });
};