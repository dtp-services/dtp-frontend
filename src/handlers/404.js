const $Templates = require('../modules/templates');

module.exports = function (app) {
  app.get('/*', (req, res) => {
    const p404 = $Templates.render('page-404');

    res.status(404).end(p404);
  });
};