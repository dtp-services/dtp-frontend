const $Admin = require('../modules/admin-action');

module.exports = async function (req) {
  const userFrom = req.session.user.id;
  const postPath = req.body.path;
  const reason = req.body.reason;
  const blockAuthor = req.body.blockAuthor;

  await $Admin.query('deletePost', {
    userFrom,
    path: postPath,
    reason,
    blockAuthor
  });

  return {ok: true};
  
};