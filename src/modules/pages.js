class Pages {
  static async render (name, params) {
    const controller = require('../page-controllers/' + name);

    return await controller(params);
  }
}

module.exports = Pages;
