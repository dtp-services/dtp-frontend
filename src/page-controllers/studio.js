const $Templates = require('../modules/templates');
const $API = require('../modules/api');
const $config = require('config');

const _ = require('lodash');

module.exports = async function (params) {
  const user = params.session.user;
  const pageParams = {
    username: user.first_name + ' ' + user.last_name,
    first_name: user.first_name,
    user_id: user.id,
    popup_publish_post: $Templates.render('popup-publish-post'),
    popup_accounts_manager: $Templates.render('popup-accounts-manager'),
    popup_account_members: $Templates.render('popup-account-members'),
    popup_account_info: $Templates.render('popup-account-info'),
    popup_import_telegraph: $Templates.render('popup-import-telegraph'),
    popup_author_url: $Templates.render('popup-author-url', {
      shortHost: $config.get('services.shortHost.url')
    })
  };
  
  if (user.photo) {
    const photo =  user.photo[0].file_id;
    pageParams.photo = $config.get('services.files.url') + '/photo/' + photo;
  } else {
    pageParams.symbolPhoto = user.first_name[0];
  }

  const page = $Templates.render('page-studio', pageParams);

  const base = $Templates.render('page-base', {
    title: 'Studio',
    page
  });

  return base;
};