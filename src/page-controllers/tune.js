const $Templates = require('../modules/templates');
const $API = require('../modules/api');

const _ = require('lodash');

module.exports = async function (params) {
  const user = params.session.user;
  const page = $Templates.render('page-tune', {
    first_name: user.first_name,
    last_name: user.last_name
  });

  const base = $Templates.render('page-base', {
    title: 'Tune the Trending',
    page,
    nightMode: params.cookies['n-m'] || false
  });

  return base;
};