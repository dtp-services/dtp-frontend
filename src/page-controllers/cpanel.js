const $Templates = require('../modules/templates');
const $API = require('../modules/api');

const _ = require('lodash');

module.exports = async function (params) {
  const user = params.session.user;
  const page = $Templates.render('page-admin', {
    first_name: user.first_name,
    last_name: user.last_name
  });

  return page;
};