const $Templates = require('../modules/templates');
const $API = require('../modules/api');
const $TelegraphAPI = require('../modules/telegraph-api');

const contentTelegraph = require('../functions/telegraph-content-replace').contentTelegraph;

const _ = require('lodash');

module.exports = async function (params) {
  const user = params.session.user;
  const check = params.check;
  const post = await $TelegraphAPI.query('getPage', {
    path: params.path
  })
    .then((data) => {
      return data.result;
    });

  post.title = post.title.replace(/'/g, '’');
  user.first_name = user.first_name.replace(/'/g, '’');
  if (check.account) {
    check.account.name = check.account.name.replace(/'/g, '’');
  }

  if (post.image_url) {
    post.image_url = contentTelegraph(post.image_url);
  }

  const page = $Templates.render('page-telegraph-publish', {
    account: check.account,
    last_name: user.last_name,
    language: check.language,
    path: params.path,
    image: post.image_url,
    title: post.title,
    description: post.description,
    asName: check.account ? check.account.name : user.first_name,
    popup_publish_post: $Templates.render('popup-publish-post'),
    accountId: check.account ? `'${check.account.id}'` : user.id
  });

  const base = $Templates.render('page-base', {
    title: post.title,
    page
  });

  return base;
};