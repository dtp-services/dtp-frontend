var Search = {};

Search.lastQuery = false;
Search.lastQueryData = {};
Search.lastOffset = 0;
Search.inputEl = $('#popup-search-input #query');
Search.paginationBox = $('#pagination-box');

Search.setResultOffset = function (offset) {
  Search.lastOffset = offset;
  Search.go(Search.lastQueryData);
};

Search.go = function (data) {
  Popup.close();
  Search.inputEl.blur();

  const _query = data.query || data.tags;

  if (_query !== Search.lastQuery) {
    Search.lastOffset = 0;
    Search.lastQuery = data.query || data.tags;
    Search.lastQueryData = data;
  }

  var params = {
    offset: Search.lastOffset || 0
  };

  if (data.query) {
    params.query = data.query;
  } else if (data.tags) {
    params.tags = data.tags;
  } else {
    return;
  }

  Search.listBox.html(App.spinner);
  Search.paginationBox.html('');

  $(document).scrollTop(0);

  ( new Promise(function (resolve, reject) {
    $.ajax({
      url: '/ajaxRender/search-post',
      method: 'post',
      data: JSON.stringify(params),
      dataType: 'json',
      contentType: 'application/json',
      success: resolve,
      error: reject
    });
  }) )
    .then(function (result) {
      if (!result.ok) {
        throw result;
      }

      Search.renderResult(result.html);

      if (result.pagination) {
        Search.paginationBox.html(result.pagination);
      } else {
        Search.paginationBox.html('');
      }
    })
    .catch(function (err) {
      console.error(err);
    });
};

Search.listBox = $('#content #list');

Search.renderResult = function (html) {
  Search.listBox.html(html);
  NightMode.check();

  // hide
  $('#case #right-icons #tune-elem').hide();
  $('#hash-tags-box').hide();
  // show
  $('#case #right-icons #trending-elem').show();

  const caseImg = $('#case #case-img');
  const caseTitle = $('#case #case-title');

  caseImg.attr('src', '/images/search-filled-500-black.png');
  caseImg.attr('src-night', '/images/search-filled-500-white.png');
  caseImg.addClass('img-nm');
  caseTitle.html('');
};

Search.goToPage = function (val) {
  if (val && val.match(/(telegra.ph|graph.org)\/.+/)) {
    const path = val.match(/(telegra.ph|graph.org)\/(.+)/)[2];

    window.location.href = '/gt/' + path;
  } else {
    alert('Telegra.ph link is not found!');
  }
};