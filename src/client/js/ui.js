$(document).ready(function () {
  DUI.init();
});

var DUI = {};

DUI.init = function () {
  $('.ui-text-switcher').each(function () {
    DUI.initTextSwitcher(this);
  });
};

DUI.initTextSwitcher = function (el) {
  var box = $(el);
  var boxId = box.attr('id');
  var changeFnName = box.attr('x-change');
  var saveValue = box.attr('save-value');
  var changeFn = changeFnName.split('.').reduce(function (r, path) {
    r = r[path];

    return r;
  }, window);

  box.find('.butt').each(function () {
    var butt = $(this);

    butt.on('click', function () {
      var butt = $(this);
      var value = butt.attr('data');

      changeFn(value);

      box.find('.butt').removeClass('active');
      butt.addClass('active');

      if (saveValue) {
        localStorage.setItem('ui-text-switcher-' + boxId, value);
      }
    });
  });

  if (saveValue) {
    var last = localStorage.getItem('ui-text-switcher-' + boxId);

    if (!last) {
      return;
    }

    box.find('.butt[data="' + last + '"]').click();    
  }
}
