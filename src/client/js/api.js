var API = {};

API.host = window.location.protocol + '//api.' + window.location.hostname;

API.query = function (method, params) {
  var token = $.cookie('dtptoken');

  if (token) {
    if (!params) {
      params = {};
    }

    if (params.token !== null) {
      params.token = token;
    }
  }

  return new Promise(function (resolve, reject) {
    $.ajax({
      method: 'post',
      url: API.host + '/' + method,
      data: JSON.stringify(params),
      dataType: 'json',
      contentType: 'application/json',
      success: resolve,
      error: reject
    });
  });
};