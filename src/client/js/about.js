$(document).ready(function (){
  var windowHeight = window.innerHeight;

  $('#slide-1').height(windowHeight);

  var imgMainLogo = $('#main-logo');
  var imgHeight = imgMainLogo.height();
  var top = Math.round((Number(windowHeight) - Number(imgHeight)) / 2) - 50;

  imgMainLogo.css({'marginTop': top + 'px'});

});