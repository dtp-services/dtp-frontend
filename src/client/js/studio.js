$(document).ready(function () {
  if (window.location.pathname === '/cs') {
    CStudio.loadAccounts();

    var header = $('#header-cs');
    var borderClass = 'with-border';
    var topTrigger = 79;
    var added;

    $(window).scroll(function () {
      var top = $(this).scrollTop();

      if (top > topTrigger && !added) {
        added = true
        header.addClass(borderClass);
      } else if (top < topTrigger && added) {
        added = false;
        header.removeClass(borderClass);
      }
    });
  }
});

var CStudio = {};

CStudio.accountsInfo = {};
CStudio.currentAccountId = false;
CStudio.currentAccountTitle = false;
CStudio.sourceTelegraph = false;
CStudio.pageBox = $('#creative-studio-page');
CStudio.sourceTelegraphBox = $('#from-telegraph-check');
CStudio.accountsMenuBox = CStudio.pageBox.find('#menu #dinamyc-component');
CStudio.accountTitle = CStudio.pageBox.find('#cs-content #account-title #data');
CStudio.postsList = CStudio.pageBox.find('#cs-content #list');
CStudio.postsListPagination = CStudio.pageBox.find('#cs-content #pagination-box');
CStudio.searchPostInput = $('#post-search');

CStudio.loadAccounts = function () {
  const cache = localStorage.getItem('csAccounts') || '';

  CStudio.accountsMenuBox.html(cache);

  ( new Promise(function (resolve, reject) {
    $.ajax({
      url: '/ajaxRender/group-accounts',
      method: 'post',
      data: JSON.stringify({}),
      dataType: 'json',
      contentType: 'application/json',
      success: resolve,
      error: reject
    });
  }) )
    .then(function (result) {
      if (!result.ok) {
        throw result;
      }

      CStudio.accountsInfo = result.json;
      CStudio.accountsMenuBox.html(result.html);

      localStorage.setItem('csAccounts', result.html);
    })
    .catch(function (err) {
      console.error(err);
    });
};

CStudio.setSourceTelegraph = function (From) {
  CStudio.sourceTelegraph = From === 'telegraph';

  if (CStudio.currentAccountId) {
    CStudio.getPosts();
    CStudio.listOffset = 0;
    CStudio.searchPostInput.val('');
  }
};

CStudio.getPostsByAccount = function (accountId, title) {
  title = title.replace(/<(?:.|\n)*?>/gm, '');
  CStudio.currentAccountId = accountId;
  CStudio.currentAccountTitle = title;
  CStudio.accountTitle.html(title + '<span>posted by</span>');
  CStudio.listOffset = 0;
  CStudio.getPosts();
  CStudio.pageBox.find('#cs-content #cpanel').show();
  CStudio.pageBox.find('#cs-content #account-title').show();
  CStudio.searchPostInput.val('');
};

CStudio.listOffset = 0;
CStudio.lastQuery = '';
CStudio.getPosts = function (query) {
  if (query !== CStudio.lastQuery) {
    CStudio.lastQuery = query;
    CStudio.listOffset = 0;
  }

  var options = {
    account_id: CStudio.currentAccountId,
    offset: CStudio.listOffset
  };

  if (typeof options.account_id === 'string') {
    CStudio.sourceTelegraphBox.show();
    $('#login-telegraph-link').attr('href', '/telegraph/' + CStudio.currentAccountId).show();
    options.telegraph = CStudio.sourceTelegraph;
  } else {
    $('#login-telegraph-link').hide();
    CStudio.sourceTelegraphBox .hide();
  }

  if (query) {
    options.query = query;
  }

  CStudio.postsList.html(App.spinner);
  CStudio.postsListPagination.html('');
  
  $(document).scrollTop(0);

  ( new Promise(function (resolve, reject) {
    $.ajax({
      url: '/ajaxRender/posts-by-account',
      method: 'post',
      data: JSON.stringify(options),
      dataType: 'json',
      contentType: 'application/json',
      success: resolve,
      error: reject
    });
  }) )
    .then(function (result) {
      if (!result.ok) {
        throw result;
      }

      CStudio.postsList.html(result.html);

      if (result.pagination) {
        CStudio.postsListPagination.html(result.pagination);
      } else {
        CStudio.postsListPagination.html('');
      }

      if (window.innerWidth > 800) {
        CStudio.searchPostInput.focus();
      }
    })
    .catch(function (err) {
      if (err.error.code === 7) {
        CStudio.postsList.html('¯_(ツ)_/¯');
        return alert('You lost access to account...');
      }

      console.error(err);
    });
};

CStudio.setAccountPostsOffset = function (offset) {
  CStudio.listOffset = offset;
  CStudio.getPosts(CStudio.lastQuery);
};

CStudio.postLanguage = 'other';
CStudio.postWords = [];
CStudio.languagePostBox = $('#popup-publish-post #data #language-box');
CStudio.keywordsPostBox = $('#popup-publish-post #data #keywords-box #words');

CStudio.setPostLanguage = function (id) {
  const lang = id.split('lang-').pop();

  CStudio.postLanguage = lang;

  CStudio.renderCreatePost();
};

CStudio.addPostWord = function (input) {
  const wordRegExp = /#?([а-яА-Я\w0-9])+/g;
  const text = input.value;
  const words = text.match(wordRegExp);

  if (( CStudio.postWords.length + words.length ) > 10) {
    return alert('Words are too much!');
  }

  words.forEach(function (w) {
    if (!CStudio.postWords.find((item) => item.toLowerCase() === w.toLowerCase())) {
      CStudio.postWords.push(w);
    }
  });

  input.value = '';
  input.blur();

  CStudio.renderCreatePost();

  $('#popup-publish-post #data #keywords-box #keywords-input').focus();
};

CStudio.renderCreatePost = function () {
  CStudio.languagePostBox.find('.lang').removeClass('hover');

  if (CStudio.postLanguage) {
    CStudio.languagePostBox.find('#lang-' + CStudio.postLanguage).addClass('hover');
  }

  const WordTemplate = Hogan.compile('<div class="word" onclick="CStudio.deletePostWord(this)">{{w}}</div>');

  CStudio.keywordsPostBox.html(CStudio.postWords.reduce(function (r, w) {
    r += WordTemplate.render({ w: w });

    return r;
  }, ''));
};

CStudio.deletePostWord = function (obj) {
  const word = $(obj).html();

  CStudio.postWords = CStudio.postWords.filter(function (w) {
    return !(w === word);
  });

  CStudio.renderCreatePost();
}

CStudio.afterPublishURL = false;
CStudio.openPublish = function (data, e) {
  e && e.preventDefault();
  
  Popup.open('popup-publish-post');
  CStudio.postLanguage = '';
  CStudio.postWords = [];
  CStudio.renderCreatePost();

  if (data.account_id) {
    CStudio.currentAccountId = typeof data.account_id === 'string' ? data.account_id : undefined;
  }

  if (data.afterGo) {
    CStudio.afterPublishURL = window.location.href;
  } else {
    CStudio.afterPublishURL = false;
  }

  if (data.title) {
    data.title = data.title.replace(/<(?:.|\n)*?>/gm, '');
  }

  if (data.authorName) {
    data.authorName = data.authorName.replace(/<(?:.|\n)*?>/gm, '');
  }

  $('#popup-publish-post #box #title span').html(CStudio.currentAccountTitle || data.authorName);
  $('#popup-publish-post #link #tit').html(data.title);
  $('#popup-publish-post #link a').html(data.link).attr('href', data.link);
  $('#popup-publish-post #loading').html('');
  $('#popup-publish-post #language-loading').html(App.spinner);

  const path = data.link.split('.ph/').pop();

  API.query('checkPost/' + path, {
    token: null
  })
    .then(function (data) {
      $('#popup-publish-post #language-loading').html('');

      if (!data.ok) {
        throw data;
      }

      if (data.result.exists) {
        throw {error: {code: 1}};
      }

      CStudio.setPostLanguage(data.result.language);
      $('#popup-publish-post #keywords-input').focus();
    })
    .catch(function (err) {
      if (err.error.code === 5) {
        return alert('This post is too small to publish.');
      } else if (err.error.code === 1) {
        return alert('Sorry, this post already exists!');
      }

      $('#popup-publish-post #language-loading').html('');

      alert('Server Error!');

      console.error(err);
    });
};

CStudio.publishPost = function () {
  const lang = CStudio.postLanguage;
  const words = CStudio.postWords;

  if (!lang) {
    return alert('Please select language!');
  }

  if (!words.length) {
    return alert('Please enter keywords!');
  }

  const link = $('#popup-publish-post #link a').attr('href');

  $('#popup-publish-post #loading').html(App.spinner);

  API.query('dropPost', {
    path: link,
    tags: words,
    language: lang,
    account_id: CStudio.currentAccountId
  })
    .then(function (data) {
      if (!data.ok) {
        throw data;
      }

      if (CStudio.afterPublishURL) {
        return window.location.href = CStudio.afterPublishURL;
      } else {
        CStudio.getPosts(CStudio.lastQuery);
      }

      Popup.close();
    })
    .catch(function (err) {
      $('#popup-publish-post #loading').html('');

      if (err.error.code === 5) {
        return alert('This post is too small to publish.');
      } else if (err.error.code === 1) {
        return alert('Sorry, this post already exists!');
      } else if (err.error.code === 11) {
        return alert('You can not post content on this resource. Please contact support.');
      }

      console.error(err);
      alert('Server Error!');
    });
};

CStudio.userId = false;
CStudio.openAccountManager = function (userId) {
  CStudio.userId = userId;
  Popup.open('popup-accounts-manager');
  CStudio.renderAccountManager();
};

CStudio.accountManagerList = $('#popup-accounts-manager #list');
CStudio.renderAccountManager = function () {
  CStudio.accountManagerList.prepend(App.spinner);
  ( new Promise(function (resolve, reject) {
    $.ajax({
      url: '/ajaxRender/manage-accounts-list',
      method: 'post',
      data: JSON.stringify({}),
      dataType: 'json',
      contentType: 'application/json',
      success: resolve,
      error: reject
    });
  }) )
    .then(function (result) {
      if (!result.ok) {
        throw result;
      }

      CStudio.accountManagerList.html(result.html);
    })
    .catch(function (err) {
      console.error(err);
      alert('Server Error!');
    });
};

CStudio.saveAccountHandler = false;
CStudio.saveAccountActionTitle = $('#popup-account-info #title #action');
CStudio.openCreateAccount = function () {
  CStudio.saveAccountActionTitle.html('New');
  Popup.open('popup-account-info', true);
  $('#import-description').show();
  Object.keys(CStudio.accountInfoForm).forEach(function (item) {
    CStudio.accountInfoForm[item].val('');
  });
  CStudio.saveAccountHandler = CStudio.createAccount;

  $('#popup-account-info #form input')[0].focus();
};

CStudio.onSaveAccountInfo = function (transfer) {
  if (CStudio.saveAccountHandler) {
    CStudio.saveAccountHandler(transfer);
  }
}

CStudio.accountInfoFormBox = $('#popup-account-info #form');
CStudio.accountInfoForm = {
  shortName: CStudio.accountInfoFormBox.find('#v-short-name'),
  authorName: CStudio.accountInfoFormBox.find('#v-author-name'),
  authorURL: CStudio.accountInfoFormBox.find('#v-author-url')
};

CStudio.createAccount = function (transfer) {
  if (transfer) {
    var tokenValue = $('#popup-import-telegraph #token-value');
    var token = tokenValue.val();

    if (!token || token.length < 20) {
      return alert('Please enter token!');
    }

    return API.query('createAccount', {
      transfer: token
    })
    .then(function (data) {
      if (!data.ok) {
        throw data;
      }

      Popup.close('popup-account-info');
      Popup.close('popup-import-telegraph');
      CStudio.renderAccountManager();
      CStudio.loadAccounts();
    })
    .catch(function (err) {
      if (err.error.code === 8) {
        return alert('Too many accounts!');
      } else if (err.error.code === 11) {
        return alert('You can not post content on this resource. Please contact support.');
      } else if (err.error.code === 7) {
        return alert('Unfortunately, this account can not be transferred to us.');
      } else if (err.error.code === 12) {
        return alert('The specified name is already taken!');
      }

      console.error(err);
      alert('Server Error!');
    });
  }

  var form = {
    short_name: CStudio.accountInfoForm.shortName.val(),
    author_name: CStudio.accountInfoForm.authorName.val(),
    author_url: CStudio.accountInfoForm.authorURL.val()
  };

  var regExpnoEmpty = /^(\w|[а-я])+/i;

  if (!form.short_name || !form.short_name.match(regExpnoEmpty)) {
    return alert('Please enter short name!');
  }

  if (form.author_url && !form.author_url.match(/(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-@]*)*\/?$/)) {
    return alert('Incorrect author URL');
  }

  API.query('createAccount', form)
    .then(function (data) {
      if (!data.ok) {
        throw data;
      }

      Popup.close('popup-account-info');
      CStudio.renderAccountManager();
      CStudio.loadAccounts();
    })
    .catch(function (err) {
      if (err.error.code === 8) {
        return alert('Too many accounts!');
      } else if (err.error.code === 11) {
        return alert('You can not post content on this resource. Please contact support.');
      } else if (err.error.code === 12) {
        return alert('The specified name is already taken!');
      }

      console.error(err);
      alert('Server Error!');
    });
};

CStudio.currentManageAccountId = false;
CStudio.inviteBox = $('#popup-account-members #account-invite');
CStudio.openManageAccount = function (id) {
  CStudio.currentManageAccountId = id;
  Popup.open('popup-account-members', true);
  CStudio.renderAccountMembers();
  CStudio.inviteBox.hide();


  var accountItem = CStudio.accountsInfo.find(function (acc) {
    return acc.id === id;
  });

  var editButton = $('#popup-account-members button#edit');

  if (accountItem.access === 2) {
    editButton.show();
  } else {
    editButton.hide();
  }
};

CStudio.accountMembersBox = $('#popup-account-members #list');
CStudio.renderAccountMembers = function () {
  var accountId = CStudio.currentManageAccountId;

  if (!accountId) {
    console.log('account id is not found');
    return;
  }

  CStudio.accountMembersBox.prepend(App.spinner);

  ( new Promise(function (resolve, reject) {
    $.ajax({
      url: '/ajaxRender/account-members-list',
      method: 'post',
      data: JSON.stringify({
        account_id: accountId
      }),
      dataType: 'json',
      contentType: 'application/json',
      success: resolve,
      error: reject
    });
  }) )
    .then(function (result) {
      if (!result.ok) {
        throw result;
      }

      CStudio.accountMembersBox.html(result.html);
    })
    .catch(function (err) {
      console.error(err);
      alert('Server Error!');
    });
};

CStudio.openEditAccount = function () {
  var accountId = CStudio.currentManageAccountId;

  CStudio.saveAccountActionTitle.html('Edit');
  Object.keys(CStudio.accountInfoForm).forEach(function (item) {
    CStudio.accountInfoForm[item].val('');
  });
  CStudio.saveAccountHandler = CStudio.editAccount;

  Popup.open('popup-account-info', true);
  $('#import-description').hide();

  CStudio.accountInfoForm.shortName.val('...');
  CStudio.accountInfoForm.authorName.val('...');
  CStudio.accountInfoForm.authorURL.val('...');

  $('#popup-account-info #form input')[0].focus();

  API.query('getAccount/' + accountId)
    .then(function (data) {
      if (!data.ok) {
        throw data;
      }

      CStudio.accountInfoForm.shortName.val(data.result.short_name);
      CStudio.accountInfoForm.authorName.val(data.result.author_name);
      CStudio.accountInfoForm.authorURL.val(data.result.author_url);
    })
    .catch(function (err) {
      if (err.error.code === 7) {
        Popup.close('popup-account-info');
        CStudio.renderAccountManager();
        CStudio.loadAccounts();
        
        return alert('You lost access to account...');
      }

      console.error(err);
      alert('Server Error');
    });
};

CStudio.editAccount = function () {
  var accountId = CStudio.currentManageAccountId;

  var form = {
    short_name: CStudio.accountInfoForm.shortName.val(),
    author_name: CStudio.accountInfoForm.authorName.val(),
    author_url: CStudio.accountInfoForm.authorURL.val()
  };

  var regExpnoEmpty = /^(\w|[а-я])+/i;

  if (!form.short_name || !form.short_name.match(regExpnoEmpty)) {
    return alert('Please enter short name!');
  }

  if (form.author_url && !form.author_url.match(/(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-@]*)*\/?$/)) {
    return alert('Incorrect author URL');
  }

  API.query('editAccount/' + accountId, form)
    .then(function (data) {
      if (!data.ok) {
        throw data;
      }

      Popup.close('popup-account-info');
      CStudio.renderAccountManager();
      CStudio.loadAccounts();
    })
    .catch(function (err) {
      if (err.error.code === 7) {
        return alert('You lost access to account...');
      } else if (err.error.code === 12) {
        return alert('The specified name is already taken!');
      }

      console.error(err);
      alert('Server Error!');
    });
};

CStudio.removeAccount = function (accountId) {
  var accountItem = CStudio.accountsInfo.find(function (acc) {
    return acc.id === accountId;
  });

  if (accountItem.access === 2) {
    var message = 'After deleting the account, the access to the editing of the materials will be lost, and the rights to the published materials will pass to you. Delete?';

    if (!confirm(message)) {
      return;
    }
  }

  API.query('deleteAccount/' + accountId)
    .then(function (data) {
      if (!data.ok) {
        throw data;
      }

      Popup.close('popup-account-members');
      CStudio.renderAccountManager();
      CStudio.loadAccounts();

      if (CStudio.currentAccountId === accountId) {
        CStudio.getPostsByAccount(CStudio.userId, 'You');
      }
    })
    .catch(function (err) {
      console.error(err);
      alert('Server Error!');
    });
};

CStudio.getNewInvite = function () {
  var accountId = CStudio.currentManageAccountId;

  if (!confirm('Having received a new invite for this account, the old ones will no longer be relevant!')) {
    return;
  }

  API.query('getAccountInvite/' + accountId)
    .then(function (data) {
      if (!data.ok) {
        throw data;
      }

      CStudio.inviteBox.show().val(data.result).focus().select();
    })
    .catch(function (err) {
      console.error(err);
      alert('Server Error!');
    });
};

CStudio.deleteMember = function (userId) {
  var accountId = CStudio.currentManageAccountId;

  API.query('deleteAccountMember/' + accountId, {
    user_id: userId
  })
    .then(function (data) {
      if (!data.ok) {
        throw data;
      }

      if (userId !== CStudio.userId) {
        CStudio.renderAccountMembers();
      } else {
        Popup.close('popup-account-members');
        CStudio.renderAccountManager();
        CStudio.loadAccounts();
      }
    })
    .catch(function (err) {
      console.error(err);
      alert('Server Error!');
    });
};

CStudio.deletePost = function (controlKey, e) {
  e && e.preventDefault();

  if (!confirm('Delete this post?')) {
    return;
  }

  var accountId = CStudio.currentAccountId;
  var postBox = $('#post-' + controlKey);
  var options = {
    controlKey: controlKey
  };

  if (typeof accountId === 'string') {
    options.account_id = accountId;
  }

  API.query('deletePost', options)
    .then(function (data) {
      if (!data.ok) {
        throw data;
      }

      postBox.remove();
    })
    .catch(function (err) {
      console.error(err);
      alert('Server Error!');
    });
};

CStudio.setAuthorNameValue = $('#popup-author-url #name-value');
CStudio.setAuthorNameView = $('#popup-author-url #link-view');
CStudio.checkAuthorName = function () {
  var value = CStudio.setAuthorNameValue.val();
  var nameRegExp = /[a-z0-9_]/i;

  var trueValue = value.split('').reduce((arr, item) => {
    if (item.match(nameRegExp)) {
      arr.push(item);
    }

    return arr;
  }, []).join(''); 

  CStudio.setAuthorNameValue.val(trueValue);
  CStudio.setAuthorNameView.find('span').html(trueValue);
  CStudio.setAuthorNameView.removeClass('yes no');

  if (trueValue.length < 2) {
    return;
  }

  var url = CStudio.setAuthorNameView.text();
  CStudio.setAuthorNameView.attr('href', url);

  API.query('checkAuthorUsername', {
    username: trueValue
  })
    .then(function (data) {
      if (!data.ok) {
        throw data;
      }

      if (data.result.exists) {
        CStudio.setAuthorNameView.addClass('no');
      } else {
        CStudio.setAuthorNameView.addClass('yes');
      }
    })
    .catch(function (err) {
      console.error(err);
    });
};

CStudio.reserveAuthorUsername = function () {
  var value = CStudio.setAuthorNameValue.val();
  var nameRegExp = /[a-z0-9_]/i;

  var trueValue = value.split('').reduce((arr, item) => {
    if (item.match(nameRegExp)) {
      arr.push(item);
    }

    return arr;
  }, []).join(''); 

  CStudio.setAuthorNameValue.val(trueValue);
  CStudio.setAuthorNameView.find('span').html(trueValue);

  if (trueValue.length < 2) {
    return;
  }

  API.query('reserveAuthorUsername', {
    username: trueValue
  })
    .then(function (data) {
      if (!data.ok) {
        throw data;
      }

      var url = CStudio.setAuthorNameView.text();

      CStudio.accountInfoForm.authorURL.val(url);
      Popup.close('popup-author-url');
    })
    .catch(function (err) {
      if (err.error.code === 12) {
        return alert('The specified name is already taken!');
      }

      console.error(err);
    });
};

CStudio.logoutConfirm = function (e) {
  if (!confirm('Log out account?')) {
    return e.preventDefault();
  }
};