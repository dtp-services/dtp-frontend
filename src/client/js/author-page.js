$(document).ready(function () {
  if ($('#page-author').attr('ex') === 'true') {
    var username = window.location.pathname.match(/^\/([a-zA-Z0-9_]+)/);
  
    if (username) {
      username = username[1];
      AuthorPage.username = username;
      AuthorPage.getPosts();
    }
  }
});

var AuthorPage = {};

AuthorPage.username = false;
AuthorPage.list = $('#page-author #right-box #list');
AuthorPage.paginationBox = $('#page-author #right-box #pagination-box');
AuthorPage.listOffset = 0;
AuthorPage.getPosts = function () {
  AuthorPage.list.html(App.spinner);
  AuthorPage.paginationBox.html('');

  var params = {
    username: AuthorPage.username,
    offset: AuthorPage.listOffset
  };

  if (AuthorPage.searchQuery) {
    params.query = AuthorPage.searchQuery;
  }

  ( new Promise(function (resolve, reject) {
    $.ajax({
      url: '/ajaxRender/author-posts',
      method: 'post',
      data: JSON.stringify(params),
      dataType: 'json',
      contentType: 'application/json',
      success: resolve,
      error: reject
    });
  }) )
    .then(function (result) {
      if (!result.ok) {
        throw result;
      }

      AuthorPage.list.html(result.html);

      if (result.pagination) {
        AuthorPage.paginationBox.html(result.pagination);
      } else {
        AuthorPage.paginationBox.html('');
      }
    })
    .catch(function (err) {
      console.error(err);
    });
};

AuthorPage.setPostsOffset = function (offset) {
  AuthorPage.listOffset = offset;
  AuthorPage.getPosts();
};

AuthorPage.searchQuery = false;
AuthorPage.setSearchQuery = function (query) {
  if (!query) {
    AuthorPage.searchQuery = false;
    AuthorPage.listOffset = 0;
  }

  if (query != AuthorPage.searchQuery) {
    AuthorPage.listOffset = 0;
  }

  AuthorPage.searchQuery = query;

  AuthorPage.getPosts();
};