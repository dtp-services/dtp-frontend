var NightMode = {};

NightMode.on = function () {
  $('html, body').css({background: 'rgb(24, 24, 24)'});
  $('*').addClass('night-mode');
  $.cookie('n-m', 'on');

  $('img.img-nm').each(function () {
    var img = $(this);
    var nightUrl = img.attr('night-src');

    if (!img.attr('day-src')) {
      img.attr('day-src', img.attr('src'));
    }

    img.attr('src', nightUrl);
  });

  $('.night-hide').hide();
};

NightMode.off = function () {
  $('html, body').css({background: 'white'});
  $('*').removeClass('night-mode');
  $('.night-hide').show();
  $.removeCookie('n-m');

  $('img.img-nm').each(function () {
    var img = $(this);
    var dayUrl = img.attr('day-src');

    if (dayUrl) {
      img.attr('src', dayUrl);
    }
  });
};

NightMode.switch = function () {
  var mode = $.cookie('n-m') || 'off';

  if (mode === 'on') {
    NightMode.off();
  } else {
    NightMode.on();
  }
};

NightMode.check = function () {
  var mode = $.cookie('n-m') || 'off';

  if (mode === 'on') {
    NightMode.on();
  }
};